/**
 * An interface that guarantees all Units have all the required methods.
 * 
 * @author Benjapol Worakan 5710546577
 * @version 15.3.3
 */
public interface Unit {
	/**
	 * Convert an amount from current Unit to another Unit.
	 * 
	 * @param amount
	 *            - an amount of the Unit that is converting from
	 * @param unit
	 *            - a Unit to convert to
	 * @return the amount converted to given Unit.
	 */
	public double convertTo(double amount, Unit unit);

	/**
	 * Get the value of the Unit comparing to base Unit.
	 * 
	 * @return Value of the Unit comparing to base Unit.
	 */
	public double getValue();

	/**
	 * Get the String representation of the Unit.
	 * 
	 * @return String representation of the Unit.
	 */
	public String toString();
}
