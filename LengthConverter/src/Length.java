/**
 * A length unit that contains the name of the unit and its conversion factor
 * comparing to one meter unit.
 * 
 * @author Benjapol Worakan 5710546577
 * @version 15.3.3
 */
public enum Length implements Unit {
	// Base unit.
	METER("meter", 1.00),
	// Metric units.
	CENTIMETER("centimeter", 0.01), KILOMETER("kilometer", 1000),
	// Imperial units.
	MILE("mile", 1609.344), FOOT("foot", 0.3048),
	// Thai units.
	WA("wa", 2.0);

	/** Name of the Length unit. */
	private String name;
	/** Base value of the Length unit compared to one meter */
	private double value;

	/**
	 * Construct a new Length unit.
	 */
	private Length(String name, double value) {
		this.name = name;
		this.value = value;
	}

	@Override
	public double convertTo(double amount, Unit unit) {
		return amount * value / unit.getValue();
	}

	@Override
	public double getValue() {
		return value;
	}

	@Override
	public String toString() {
		return name;
	}
}
