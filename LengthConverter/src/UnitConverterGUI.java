import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 * A GUI for UnitConverter.
 * 
 * @author Benjapol Worakan 5710546577
 * @version 15.3.3
 */
public class UnitConverterGUI extends JFrame implements Runnable {

	/** SerialVersionUID number */
	private static final long serialVersionUID = 1L;
	/** Text fields for input and result */
	private JTextField inputField, resultField;
	/** Combo boxes for input unit and result unit */
	private JComboBox<Unit> inputUnit, resultUnit;
	/** Buttons for initiating conversion and resetting UI */
	private JButton convertButton, clearButton;
	/** A UnitConverter received from application */
	private UnitConverter unitConverter;

	/**
	 * Create a new UnitConverterGUI JFrame referencing to the received
	 * UnitConverter.
	 * 
	 * @param unitConverter
	 *            - a UnitConverter for this GUI to use
	 */
	public UnitConverterGUI(UnitConverter unitConverter) {
		super("Length Converter");
		this.unitConverter = unitConverter;
	}

	/**
	 * Invoke, initialize and run the GUI.
	 */
	@Override
	public void run() {
		initUIComponents();
		pack();
		setVisible(true);
	}

	/**
	 * Initialize GUI components.
	 */
	private void initUIComponents() {
		setDefaultCloseOperation(EXIT_ON_CLOSE);

		Container contentPane = getContentPane();
		contentPane.setLayout(new FlowLayout(FlowLayout.LEFT));

		inputField = new JTextField(10);
		resultField = new JTextField(10);

		inputUnit = new JComboBox<Unit>(unitConverter.getUnits());
		resultUnit = new JComboBox<Unit>(unitConverter.getUnits());

		convertButton = new JButton("Convert");
		clearButton = new JButton("Clear");

		contentPane.add("input", inputField);
		contentPane.add("inputUnitBox", inputUnit);
		contentPane.add("equalSign", new JLabel("="));
		contentPane.add("result", resultField);
		contentPane.add("resultUnit", resultUnit);
		contentPane.add("convertButton", convertButton);
		contentPane.add("clearButton", clearButton);

		inputField.setText("");
		resultField.setText("0.0");
		resultField.setEditable(false);

		ActionListener convertAction = new ConvertAction();

		inputField.addActionListener(convertAction);
		convertButton.addActionListener(convertAction);
		clearButton.addActionListener(new ActionListener() {
			@Override
			/**
			 * Reset the UI.
			 */
			public void actionPerformed(ActionEvent e) {
				inputField.setText("");
				resultField.setText("0.0");
				inputUnit.setSelectedIndex(0);
				resultUnit.setSelectedIndex(0);
			}
		});
	}

	/**
	 * An ActionListener that will initiate the conversion process when
	 * triggered.
	 * 
	 * @author Benjapol Worakan 5710546577
	 * @version 15.3.3
	 */
	class ConvertAction implements ActionListener {
		/**
		 * Initate conversion.
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			double inputValue;
			Unit from, to;
			try {
				inputValue = Double.valueOf(inputField.getText().trim());
				from = (Unit) inputUnit.getSelectedItem();
				to = (Unit) resultUnit.getSelectedItem();

				resultField.setText(Double.toString(unitConverter.convert(
						inputValue, from, to)));
			} catch (Exception ex) {
				errorAlert("Invalid input(s). "
						+ "Please check your input(s) and try again.", ex);
			}
		}
	}

	/**
	 * Display an error dialog and an Exception stack trace.
	 * 
	 * @param message
	 *            - an error message to be displayed
	 * @param e
	 *            - Exception occurred, this is optional, if there's no
	 *            Exception just use null
	 */
	private void errorAlert(String message, Exception e) {
		if (e != null) {
			JOptionPane.showMessageDialog(this,
					String.format("%s\n\n%s", message, e), "Error",
					JOptionPane.ERROR_MESSAGE);
		} else {
			JOptionPane.showMessageDialog(this, message, "Error",
					JOptionPane.ERROR_MESSAGE);
		}
	}
}