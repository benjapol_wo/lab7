import java.awt.EventQueue;

/**
 * Main application class for UnitConverter. Will create and invoke
 * UnitConverter and GUI.
 * 
 * @author Benjapol Worakan 5710546577
 * @version 15.3.3
 */
public class Main {
	/**
	 * Invoke the application, create a GUI using a UnitConverter, and run it.
	 * 
	 * @param args
	 *            - not used
	 */
	public static void main(String[] args) {
		final Runnable GUI = new UnitConverterGUI(new UnitConverter());
		EventQueue.invokeLater(GUI);
	}
}
