/**
 * 
 * @author Benjapol Worakan 5710546577
 * @version 15.3.3
 */
public class UnitConverter {
	/**
	 * Convert a value from one unit to another unit.
	 * 
	 * @param value
	 *            - a value to be converted
	 * @param from
	 *            - a Unit to convert from
	 * @param to
	 *            - a Unit to convert to
	 * @return The value that is converted to the provided Unit.
	 */
	public double convert(double value, Unit from, Unit to) {
		return from.convertTo(value, to);
	}

	/**
	 * Get all Unit(s) available.
	 * 
	 * @return Unit(s) available.
	 */
	public Unit[] getUnits() {
		return Length.values();
	}
}
